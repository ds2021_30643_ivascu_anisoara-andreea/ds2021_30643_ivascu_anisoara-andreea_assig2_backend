package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.Sensor;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface SensorRepository extends JpaRepository<Sensor, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Sensor> findByDescription(String description);

    @Query(value = "SELECT s " +
            "FROM Sensor s " +
            "WHERE s.id = :id")
    Optional<Sensor> findWithId(@Param("id") UUID id);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT s " +
            "FROM Sensor s " +
            "WHERE s.description = :description")
    Optional<Sensor> findSeniorsByUsername(@Param("description") String description);


}

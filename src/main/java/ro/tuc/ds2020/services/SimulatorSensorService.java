package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.repositories.SensorRepository;
import ro.tuc.ds2020.repositories.SimulatorSensorRepository;

@Service
public class SimulatorSensorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SensorService.class);
    private final SimulatorSensorRepository simulatorSensorRepository;

    @Autowired
    public SimulatorSensorService(SimulatorSensorRepository simulatorSensorRepository) {
        this.simulatorSensorRepository = simulatorSensorRepository;
    }
}
